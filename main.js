class EuroApp extends HTMLElement {
  constructor() {
    super();
  }
  
  connectedCallback() {
    //code here
    window.addEventListener("checkVal", function(e) {
    $.ajax({
      type: "post",
      url: "https://localhost:44346/api/number",
      data: 
      {
       value: e.detail
      },
      success: (response) => {
        // handle response
        document.querySelector("p").innerText = response;
      },
      error: (err) => {
        // handle err
        document.querySelector("p").innerText = JSON.stringify(err.responseJSON.error);
      }
    });

    });
  }
  
  disconnectedCallback() {
    //code here
  }
  
  attributeChangedCallback(attrName, oldVal, newVal) {
    //code  here
  }
  
}

customElements.define("euro-app", EuroApp);

function test() {
  let data = document.querySelector("input").value;
  window.dispatchEvent(new CustomEvent("checkVal", {
    detail: data
  }));
}
